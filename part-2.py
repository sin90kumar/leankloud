# -*- coding: utf-8 -*-
"""
Created on Sat Jul  3 22:22:54 2021

@author: Dell XPS Premium
"""
import csv
import sys

filename = sys.argv[0]
flag = True

head, marks = [],[]
math_max, bio_max, eng_max, phy_max, chem_max, hin_max = [0, 0],[0, 0],[0, 0],[0, 0],[0, 0],[0, 0]
first_rank, second_rank, third_rank = [0, 0],[0, 0],[0, 0]

#importing the dataset
with open('Student_marks_list.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        if flag:
            head = row
            flag = False
        else:
            row_wise = [row[0]]
            total = 0
            for i in range(1, len(row)):
                row_wise.append(int(row[i]))
                total += int(row[i])
            row_wise.append(total)
            marks.append(row_wise)

#Find the topper in each subject.

for i in range(len(marks)):
    if marks[i][1] > math_max[0]:
        math_max[0] = marks[i][1]
        math_max[1] = marks[i][0]
    if marks[i][2] > bio_max[0]:
        bio_max[0] = marks[i][2]
        bio_max[1] = marks[i][0]
    if marks[i][3] > eng_max[0]:
        eng_max[0] = marks[i][3]
        eng_max[1] = marks[i][0]
    if marks[i][4] > phy_max[0]:
        phy_max[0] = marks[i][4]
        phy_max[1] = marks[i][0]
    if marks[i][5] > chem_max[0]:
        chem_max[0] = marks[i][5]
        chem_max[1] = marks[i][0]
    if marks[i][6] > hin_max[0]:
        hin_max[0] = marks[i][6]
        hin_max[1] = marks[i][0]

#Find the top 3 students in the class, based on their marks in all subjects.

for i in range(len(marks)):
    if marks[i][7] > third_rank[0]:
        if marks[i][7] > second_rank[0]:
            if marks[i][7]>first_rank[0]:
                second_rank[0] = first_rank[0]
                second_rank[1] = first_rank[1]
                first_rank[0] = marks[i][7]
                first_rank[1] = marks[i][0]
            else:
                third_rank[0] = second_rank[0]
                third_rank[1] = second_rank[1]
                second_rank[0] = marks[i][7]
                second_rank[1] = marks[i][0]
        else:
            third_rank[0] = marks[i][7]
            third_rank[1] = marks[i][0]

print("Topper in Maths is ",math_max[1])
print("Topper in Biology is ",bio_max[1])
print("Topper in English is ",eng_max[1])
print("Topper in Physics is ",phy_max[1])
print("Topper in Chemistry is ",chem_max[1])
print("Topper in Hindi is ",hin_max[1])

print("\nBest students in the class are {}, {}, {}".format(first_rank[1], second_rank[1], third_rank[1]))